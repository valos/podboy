     ____           _ _
    |  _ \ ___   __| | |__   ___  _   _
    | |_) / _ \ / _` | '_ \ / _ \| | | |
    |  __/ (_) | (_| | |_) | (_) | |_| |
    |_|   \___/ \__,_|_.__/ \___/ \__, |
                                   |___/

Description
===========

Podboy is a podcast aggregator/player written in Python/Elementary.

Its interface aims to be easy and finger friendly.
It uses Feedparser Python module for downloading/parsing podcast feeds,
Gstreamer for playing episodes and SQLite for storing data.

https://gitlab.com/valos/podboy


Features
========
- Designed to be finger friendly, fast, and easy to use.
- RSS, Atom feeds
- User interface in Elementary
- Auto-suspend is disabled during playing
- Playing is stopped on incoming GSM call
- Bluetooth A2DP support
- Import from OPML file


Authors
=======
- Valéry Febvre <vfebvre@easter-eggs.com>
- Emmanuel Raviart <eraviart@easter-eggs.com>


Contributors
============
- Benjamin Deering <ben_deering@swissmail.org>


Requirements
============
Have a look to podboy.bb or pkg/control files.


Report Bugs
===========
If there is something wrong, please use to the Google code issues:
https://gitlab.com/valos/podboy/issues
Report a bug or look there for possible workarounds.


Credits
=======
- Tango Desktop Project for icons (http://tango.freedesktop.org/)


Version History
===============

2012-04-12 - Version 1.7.4
--------------------------
- Release due to Elementary API changes

2011-02-04 - Version 1.7.3
--------------------------
- Minor bugfixes release.
- Removed experimental code: additional capsfilter element in GStreamer
  pipeline to always resample audio automatically at 44100Hz
- Filter some characters in podcasts folder name to avoid possible
  problems with some filesystem like VFAT.

2010-11-22 - Version 1.7.2
--------------------------
- Major bugfixes release due to Elementary API changes in List, Toolbar
  and Icon widgets (python-elementary revision >= 53901).

2010-05-07 - Version 1.7.1
--------------------------
- Podboy's birthday. One year ago, first version of Podboy was released.

- Duration of episodes: saved duration provided by GStreamer if it's
  missing or different from the one provided in feed, always display
  duration in format HH:MM:SS.

- Size of episodes: saved size of episode provided in feed.
  You can finally show episodes sizes in details of episodes.

- Download of episodes: retrieved correct name of media file when it's
  in query string of url, fixed wget sub-process call for complex urls
  (with a query string).

2010-05-03 - Version 1.7.0
--------------------------
- Show number of episodes available per podcasts in page Downloads.

- Show number of episodes not played and number of episodes available
  per podcasts in page Episodes.

- In page Downloads, episodes that have been toggled as ignore are not
  anymore included in totals.

- Minor improvement in the update of podcasts.

2010-04-27 - Version 1.6.2
--------------------------
- Improved/fixed download of episodes:
  * used ecore Exe module instead of python subprocess module
  * properly check ends (success or failure) of wget sub-processes
  * displayed a summary when the download of at least one episode failed

2010-04-15 - Version 1.6.1
--------------------------
- Bugfix release: a bug in Settings page prevented its opening.
  It does happen only with a fresh install when the media folder was not
  yet defined.
  Thanks to Giuseppe Palmiotto for report.

2010-04-15 - Version 1.6.0
--------------------------
- Enabled OGG audio format support.
  Ogg support has been added 3 months ago, but it was so far disabled
  due to a bug in GStreamer oggdemux pipeline element (bug fixed in
  version 0.10.28).

- New internal widget: Notify dialog.
  It is used to notify user when an operation, that can take some time,
  ends.

2010-03-05 - Version 1.5.0
--------------------------
- New feature: played episodes that are older than a specified amount of
  days (defined in Settings) can be auto-deleted on every startup.

- New feature: episodes can now be tagged as "(un)deletable" in page
  "Episodes". All episodes with status "undeletable" will be ignored
  when auto-deletion of old episodes will be done on startup.

2010-02-26 - Version 1.4.0
--------------------------
- New feature: podcasts can be renamed.

- New feature: downloads can be canceled.

- Big code cleanup and code refactoring.

2010-02-12 - Version 1.3.4
--------------------------
- Add multi selection in lists (podcasts and episodes).

- Add a missing refresh of "Downloads" page when episodes are deleted in
  "Episodes" page.

- Little improvements here and there (sorry I don't remember where).

2010-02-02 - Version 1.3.3
--------------------------
- Lot of improvements in feed parsing (add and update).

- Now, when a "Check For Updates" is done for several podcasts, a full
  report is displayed in a dialog.

- New feature: podcasts source URLs can be edited.

2010-01-29 - Version 1.3.2
--------------------------
- Fixed a deletion bug with episodes.

- Fixed a download bug for episodes with status "ignore".

- Behavior change: when an epsiode is deleted, don't delete it anymore
  in database.
  Only the media file is deleted in filesystem. So, user can download
  the episode again if he changes of advice and if episode is still
  available. Last playing position is kept and status of episode is set
  to "ignore".

2010-01-26 - Version 1.3.1
--------------------------
- Speed up startup a bit (~1 second less).

- Fixed regression introduced in 1.3.0: some feed enclosures without
  mime-type were rejected and therefore episodes were skipped.

- Removed command line option "--import/-i".
  It's always possible to import podcasts via command line with a new
  script called 'podboy-importer'.

2010-01-25 - Version 1.3.0
--------------------------
- Add 3 new settings in a new section "Display": finger size, scaling
  factor and orientation.

- Speed up startup (~3 seconds less).

- View covers of podcasts in big size: just tap on cover in page
  "Player".

- Allow podcasts URLs which start with itpc:// and feed://

2010-01-12 - Version 1.2.0
--------------------------
- New feature: show details of episodes in page "Downloads" (like in
  page "Episodes").

- New feature: update only the selected podcast in page "Downloads".

- New feature: episodes can now be tagged as "Ignore" in page
  "Downloads". All episodes with status "ignore" will be skip when the
  downloading of all episodes of a podcast will be requested.

- New feature: import of a list of podcasts via an OPML file (new button
  "Import" in the page "Podcasts")

- Fixed occasionnaly incorrect display size of podcasts covers.

2010-01-02 - Version 1.1.0
--------------------------
- Unplayed episodes can now be identified by a yellow star.

- Unplayed/played status of episodes can be manually toggled via a new
  entry named "Toggle Played Status" in the "Actions" hoversel of
  "Episodes" page.

- Add possibility to show details of episodes before to play them via a
  new entry named "Show Details" in the "Actions" hoversel of "Episodes"
  page.

2009-12-22 - Version 1.0.1
--------------------------
- Minor bug fix release: during first run, the configuration folder was
  not created.

2009-12-21 - Version 1.0.0
--------------------------
- Added a Bluetooth A2DP support (not yet optimal, mp3 data are not sent
  directly but transmitted with SBC)

- No more config file, now settings are stored in database (in table
  "settings")

- Fixed callbacks with new python-elementary (revision >= 43900)

- Added date at the begin of episodes titles

- Used InnerWindow widget instead of Window widget for confirmation
  dialogs

- Added a confirm dialog when a single episode is deleted

- Speed up startup (~3 seconds less)

- Renamed Preferences into Settings.

- Added "About" page

- Many small fixes here and there

2009-05-26 - Version Pre-Alpha 0.0.4
------------------------------------
- Used a hover widget to delete episodes (to be homogeneous with
  downloads).

- Episodes are now downloaded by blocks (not all at the same time).
  The number of episodes by block can be modified with the constant
  SIMULTANEOUS_DOWNLOADS (default 1 to be less heavy in CPU).

- Code cleanup

2009-05-15 - Version Pre-Alpha 0.0.3
------------------------------------
- Rework GUI of downloads and episodes pages.

- Add icons to player's buttons.

- Added confirmation dialogs.

- Preliminary script to import a text file containing URLs of feed.
  Added command-line option to import podcasts.

- Factorized DB code.

2009-05-08 - Version Pre-Alpha 0.0.2
------------------------------------
- Improved seeking.

- Improved auto-suspend management.
  Used now org.freesmartphone.ousaged when org.shr.ophonekitd.Usage is
  not available.
  Auto-suspend management COULD worked now under FSO or OM2009.

2009-05-07 - Version Pre-Alpha 0.0.1
------------------------------------
- First release (Pre-Alpha).

- Subscription, unsubscription (confirmation dialog) to podcasts
- Check for new episodes
- Download of a single episode or all available episodes
- Delete an episode (confirmation dialog)
- Player: play/pause/stop, mute, sliders for playing position of
  episodes and volume
- Volume and playing positions of episodes are saved
- Auto-suspend is disabled during playing
- Playing is stopped on incoming GSM call
