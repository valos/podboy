DESCRIPTION = "A podcast aggregator/player"
HOMEPAGE = "http://code.google.com/p/podboy/"
LICENSE = "GPLv3"
AUTHOR = "Valéry Febvre <vfebvre@easter-eggs.com>"
SECTION = "x11/applications"
PRIORITY = "optional"

SRCREV = "221"
PV = "1.7.4+svnr${SRCPV}"

PACKAGE_ARCH = "all"

SRC_URI = "svn://podboy.googlecode.com/svn;module=trunk;proto=http"
S = "${WORKDIR}/trunk"

inherit distutils

FILES_${PN} += "${datadir}/podboy ${datadir}/applications/podboy.desktop ${datadir}/pixmaps/podboy.png"

RDEPENDS += "python-compression python-edbus python-elementary python-gst python-html python-netclient python-netserver python-pygobject python-sqlite3 python-subprocess gst-plugins-base-alsa gst-plugins-base-audioconvert gst-plugins-base-audioresample gst-plugin-bluetooth gst-plugins-ugly-mad gst-plugins-base-ogg gst-plugins-base-volume gst-plugins-base-vorbis"

do_compile_prepend() {
	${STAGING_BINDIR_NATIVE}/edje_cc -id ${S}/data ${S}/data/podboy.edc
}
