# -*- coding: utf-8 -*-

# Podboy -- A podcast aggregator/player
#
# Copyright (C) 2009-2012 Valéry Febvre <vfebvre@easter-eggs.com>
# http://code.google.com/p/podboy/
#
# This file is part of Podboy.
#
# Podboy is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Podboy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

APP_NAME    = 'Podboy'
APP_WM_INFO = (APP_NAME, APP_NAME.lower())
APP_VERSION = '1.7.4'

DATA_DIR = '/usr/share/podboy/'
#DATA_DIR = '../data/'

THEME_EDJ = DATA_DIR + 'podboy.edj'

STATUS_MSG_TIMEOUT = 3

SIMULTANEOUS_DOWNLOADS = 1
