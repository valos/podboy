# -*- coding: utf-8 -*-

# Podboy -- A podcast aggregator/player
#
# Copyright (C) 2009-2012 Valéry Febvre <vfebvre@easter-eggs.com>
# http://code.google.com/p/podboy/
#
# This file is part of Podboy.
#
# Podboy is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Podboy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
Podboy is a podcast aggregator/player
"""

import os, sys
import elementary

from const import *
from database import db
from libdbus import PodboyDBus
from libpodcast import PodcastEpisode
from gui import *


class Podboy(object):
    def __init__(self):
        self.delete_old_episodes()

        self.win = elementary.Window(APP_NAME, elementary.ELM_WIN_BASIC)
        self.win.title_set(APP_NAME)
        self.win.callback_destroy_add(self.quit)

        if db.get_setting('display_finger_size') is not None:
            elementary.config_finger_size_set(db.get_setting('display_finger_size'))
        if db.get_setting('display_scale') is not None:
            elementary.scale_set(db.get_setting('display_scale'))
        if db.get_setting('display_orientation') is not None:
            self.win.rotation_set(270 * int(db.get_setting('display_orientation')))

        self.confirm_dlg = ConfirmDialog(self)
        self.fs_dlg = FileselectorDialog()
        self.info_dlg = InfoDialog(self)
        self.notify_dlg = NotifyDialog(self)

        bg = elementary.Background(self.win)
        self.win.resize_object_add(bg)
        bg.size_hint_weight_set(1.0, 1.0)
        bg.show()

        box = elementary.Box(self.win)
        self.win.resize_object_add(box)
        box.size_hint_weight_set(1.0, 1.0)
        box.show()

        toolbar = elementary.Toolbar(self.win)
        toolbar.menu_parent_set(self.win)
        toolbar.homogenous_set(False)
        toolbar.size_hint_align_set(-1.0, 0)

        self.toolbar_item_podcasts = toolbar.item_append('podcasts', "Podcasts", self.show_podcasts)
        self.toolbar_item_downloads = toolbar.item_append('downloads', "Downloads", self.show_downloads)
        self.toolbar_item_episodes = toolbar.item_append('episodes', "Episodes", self.show_episodes)
        self.toolbar_item_player = toolbar.item_append('player', "Player", self.show_player)
        self.toolbar_item_settings = toolbar.item_append('settings', "Settings", self.show_settings)
        self.toolbar_item_about = toolbar.item_append('about', "About", self.show_about)

        box.pack_end(toolbar)
        toolbar.show()

        self.pager = elementary.Naviframe(self.win)
        self.pager.size_hint_weight_set(1.0, 1.0)
        self.pager.size_hint_align_set(-1.0, -1.0)
        box.pack_end(self.pager)
        self.pager.show()

        self.podcasts  = PodcastsPage(self)
        self.downloads = DownloadsPage(self)
        self.episodes  = EpisodesPage(self)
        self.player    = PlayerPage(self)
        self.settings  = SettingsPage(self)
        self.about     = AboutPage(self)

        self.dbus = PodboyDBus(self.player.stop)

        self.win.resize(480, 640)
        self.win.show()

        if db.count_podcasts():
            if db.count_episodes(downloaded = 1):
                self.show_episodes()
            else:
                self.show_downloads()
        else:
            self.show_podcasts()

    def delete_old_episodes(self):
        if db.get_setting('episodes_old_auto_delete'):
            for episode in db.get_old_episodes(db.get_setting('episodes_old_age')):
                ep = PodcastEpisode.load(episode['id'])
                ep.delete()

    def quit(self, obj):
        print "Bye bye, thx to use %s" % APP_NAME
        self.player.stop()
        db.close()
        elementary.exit()

    def show_podcasts(self, *args):
        self.toolbar_item_podcasts.selected = True
        self.podcasts.promote()

    def show_downloads(self, *args):
        self.toolbar_item_downloads.selected = True
        self.downloads.promote()

    def show_episodes(self, *args):
        self.toolbar_item_episodes.selected = True
        self.episodes.promote()

    def show_player(self, *args):
        self.toolbar_item_player.selected = True
        self.player.promote()

    def show_settings(self, *args):
        self.toolbar_item_settings.selected = True
        self.settings.promote()

    def show_about(self, *args):
        self.toolbar_item_about.selected = True
        self.about.promote()


def main():
    elementary.init()
    elementary.theme_overlay_add(THEME_EDJ)
    Podboy()
    elementary.run()
    elementary.shutdown()
    return 0

if __name__ == "__main__":
    sys.exit(main())
